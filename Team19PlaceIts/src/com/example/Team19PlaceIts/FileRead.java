package com.example.Team19PlaceIts;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mustafa on 2/15/14.
 */
public class FileRead {
    private File myFile;
    private FileInputStream fIn;
    private BufferedReader myReader;
    boolean exist;


    public FileRead(String fileName){
        File sdcard = Environment.getExternalStorageDirectory();
        try {
            myFile = new File(sdcard, fileName);
            if(myFile.exists()) exist = true;
            fIn = new FileInputStream(myFile);
            myReader = new BufferedReader(new InputStreamReader(fIn));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



    public ArrayList<String[]> readFile(StringBuilder buffer){
        ArrayList<String[]> mList = new ArrayList<String[]>();
        if(!exist) return mList;
        try {
            String line = "";
            while ((line = myReader.readLine()) != null)
            {
                buffer.append(line + "\n");
                String[] str = line.split("<%%>");
                mList.add(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mList;
    }

    public void closeReadFile() {
        try {
            myReader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            return;
        }
    }

    public void deleteFile(){
        if(this.myFile.exists()) this.myFile.delete();
    }

}
