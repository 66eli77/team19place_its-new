package com.example.Team19PlaceIts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Scanner;

import android.os.Environment;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class FileWrite {
	private File myFile;

	private FileOutputStream fOut;
	private OutputStreamWriter myOutWriter;

    public FileWrite(String fileName) {
        File sdcard = Environment.getExternalStorageDirectory();
        myFile = new File(sdcard, fileName);
        try {
            myFile.createNewFile();
            fOut = new FileOutputStream(myFile);
            myOutWriter =new OutputStreamWriter(fOut);
        } catch (IOException e) {
            e.printStackTrace();
        }

	}

	public void write(String str) {
		try {
            myOutWriter.append(str);
            myOutWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void closeWriteFile() {
		try {
            myOutWriter.close();
            fOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    public boolean equal(String str){
        return this.myFile.getName().compareTo(str) == 0;
    }



}
