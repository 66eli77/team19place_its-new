package com.example.Team19PlaceIts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.example.Team19PlaceIts.R;
import com.example.Team19PlaceIts.MainActivity;
import com.example.Team19PlaceIts.NotificationView;
import com.example.Team19PlaceIts.OnClickActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.app.TaskStackBuilder;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

// Import for notification
import android.app.Notification;
import android.app.NotificationManager;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

// Import for custom marker
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;

// Import for proximity alert
import android.location.LocationManager;
import android.location.Location;
import android.location.LocationListener;
import android.content.IntentFilter;
import android.view.View.OnClickListener;
import android.content.ContextWrapper;
import android.content.BroadcastReceiver;


public class MainActivity extends Activity implements OnMapClickListener, OnMarkerClickListener, CancelableCallback {

	private GoogleMap mMap;
	
	private List<Marker> mMarkerList = new ArrayList<Marker>();
	
	private Iterator markerIterator;
	
	// Proximity alert
	private static final float RADIUS = 804.672f;	// Proximity to location in meters
	LocationManager locationManager;
	private static final String PROXIMITY_ALERT_INTENT = 
			"com.example.Team19PlaceIts.ProximityAlert";
	
	//Notification
	private NotificationManager mNotificationManager;
	private int notificationID = 100;   
	private int numMessages = 0;
	
	// Create custom icon to display on map
	private Resources res = getResources();
	private Drawable drawable = res.getDrawable(R.drawable.marker_icon);
	public BitmapDescriptor customIcon = BitmapDescriptorFactory.fromBitmap(((BitmapDrawable) drawable).getBitmap());
	
	//for file read and write
	private FileRead fRead ;
    private FileWrite fWriter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		fRead = new FileRead("active.txt"); //creates read file from active list 
											//TODO: complete list
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setUpMapIfNeeded();
		mMap.setMyLocationEnabled(true);
		mMap.setOnMapClickListener(this);
		mMap.setOnMarkerClickListener(this);
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE); 
		
		// Restore marker from lost session
        StringBuilder buffer = new StringBuilder();
        ArrayList<String[]> strMarker = fRead.readFile(buffer);
        this.stringToMarker(strMarker);

        fRead.closeReadFile();
        fRead.deleteFile();
        fWriter = new FileWrite("active.txt"); //make file for write
        fWriter.write(buffer.toString());
					//TODO: complete list
		
		Button btnReTrack = (Button) findViewById(R.id.retrack);
			btnReTrack.setOnClickListener(new View.OnClickListener() { 
				@Override
				public void onClick(View v) {
					markerIterator = mMarkerList.iterator();
					if (markerIterator.hasNext()) {
						Marker current = (Marker) markerIterator.next();
						mMap.animateCamera(CameraUpdateFactory.newLatLng(current.getPosition()), 2000, MainActivity.this);
						current.showInfoWindow();
					}
				}
			});
			
			Button btnUpdate = (Button) findViewById(R.id.sendLocationBtn);
			 btnUpdate.setOnClickListener(new View.OnClickListener() { 
				 @Override
				 public void onClick(View v) {
					 EditText editText = (EditText) findViewById(R.id.sendLocation);
					 String geoData = editText.getText().toString();
					 String[] coordinate = geoData.split(",");
					 double latitude = Double.valueOf(coordinate[0]).doubleValue();
					 double longitude = Double.valueOf(coordinate[1]).doubleValue();
					 mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 12),2000,null);
				 }
			 });
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
     * Event Handling for individual menu item selected
     * Identify single menu item by it's id
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.action_settings:
            // Single menu item is selected do something
            // Ex: launching new activity/screen or show alert message
            Toast.makeText(MainActivity.this, "Setting is selected", Toast.LENGTH_SHORT).show();
            return true;
 
        case R.id.active_list:
            Toast.makeText(MainActivity.this, "Active list is selected", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), ListViewLoader.class));
            return true;
 
        case R.id.completed_list:
            Toast.makeText(MainActivity.this, "Completed list is selected", Toast.LENGTH_SHORT).show();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private void setUpMapIfNeeded() {
		 // Do a null check to confirm that we have not already instantiated the map.
		 if (mMap == null) {
			 mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
					 .getMap();
			 // Check if we were successful in obtaining the map.
			 if (mMap != null) {
				 // The Map is verified. It is now safe to manipulate the map.
			 }
		 }
	}
	
	
	@Override
	public void onMapClick(LatLng position) {
		final LatLng pos = position;
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("New Reminder");
		alert.setMessage("Title:");
		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);
		alert.setPositiveButton("Post", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				Toast.makeText(MainActivity.this, "Reminder placed!", Toast.LENGTH_SHORT).show();
				Marker added = mMap.addMarker(new MarkerOptions()
					.position(pos)
					.title(value)
					.snippet("Description")
					.icon(customIcon));
				mMarkerList.add(added);
				addProximityAlert(pos.latitude,pos.longitude);
			}
		});
		
		//Repost Button
		alert.setNeutralButton("Repost", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				//add cal. functionality
				String value = input.getText().toString();
				Toast.makeText(MainActivity.this, "Reminder placed!", Toast.LENGTH_SHORT).show();
				Marker added = mMap.addMarker(new MarkerOptions()
				.position(pos)
				.title(value)
				.snippet("Description"));
				mMarkerList.add(added);
			}
		 
		});
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Toast.makeText(MainActivity.this, "Reminder cancelled!", Toast.LENGTH_SHORT).show();
			}
		});
		alert.show();
	 
	 
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	
	//on mark click listener 
	
		
	public boolean onMarkerClick(Marker marker) {
		//set camera view
		//display options
		marker.showInfoWindow();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton(R.string.discard_button, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			// discard marker
		    Toast.makeText(MainActivity.this, "Reminder removed!", Toast.LENGTH_SHORT).show();
			}
		 })              
		 .setNeutralButton(R.string.repost_button, new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int id) {
		     // implement repost       	   
				 Toast.makeText(MainActivity.this, "Reminder reposted!", Toast.LENGTH_SHORT).show();              
			 }             
		 })              
		 .setNegativeButton(R.string.close_button, new DialogInterface.OnClickListener() {              
			 public void onClick(DialogInterface dialog, int id) {
				 // User cancelled the dialog
				 Toast.makeText(MainActivity.this, "No Change!", Toast.LENGTH_SHORT).show();              
			 }              
		 });
		        
			
			//calls the second intent 
		//	Intent i = new Intent(this, OnClickActivity.class);
		//	   startActivity(i);
		//	   finish(); // Call once you redirect to another activity
			return true;
		
		
	}
		
		/*
		 * The function setNotification() was created in order to make setting up notifications easier. 
		 * To get this to work, just call the function name with a context as the parameter. 
		 * Ex:setNotification(MainActivity.this);
		 */
		//add the notification
		private void setNotification(Context context) {
			//start notification, cancel, update notif. three methods
			 //start notification services, invoking the default services
			 NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
			 mBuilder.setSmallIcon(R.drawable.ic_launcher); //might have to change pixels
			// mBuilder.setLargeIcon(map_logo);
			 mBuilder.setContentTitle("New Post-It!");
			 mBuilder.setContentText("blah blah blah"); 
			 Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
			 mBuilder.setLargeIcon(b);
			 
			 //create new Intent which calls the NotificationView activity 
			 //upon user touch the screen
			 Intent resultIntent = new Intent(this, NotificationView.class); //CHANGE IT NOTIFCATIONVIEW.CLASS
			 
			 /* 
			 * The stack builder object will contain an artificial back stack for the
			 * started Activity.
			 * This ensures that navigating backward from the Activity leads out of
			 * your application to the Home screen.
			 */
			 
			 //taskbuilder for backtrack
		/*	 TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
			 
			 stackBuilder.addParentStack(NotificationView.class);
			 
			// Adds the Intent that starts the Activity to the top of the stack
			 stackBuilder.addNextIntent(resultIntent);
			 PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
			 mBuilder.setContentIntent(resultPendingIntent);
			 mBuilder.setSound(getAlarmUri()); // Creates sound
			 mBuilder.setAutoCancel(true); // Allows notification to be cancelled when user clicks it
			 
			 mNotificationManager =
				      (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);	      
			 /* notificationID allows you to update the notification later on. */	      
			 mNotificationManager.notify(notificationID, mBuilder.build());
			
		}
		//Get an alarm sound. Try for an alarm. If none set, try notification, otherwise, ringtone // Can be in any order you wish
		private Uri getAlarmUri() {
		    Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION); // Originally notification and alarm were switched, but my alarm gets annoying very fast. =b
		    if (alert == null) {
		        alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		        if (alert == null) {
		            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		        }
		    } 
		    return alert;
		}

	@Override
	public void onFinish() {
		// TODO Auto-generated method stub
		if (markerIterator.hasNext()) {
			 Marker current = (Marker) markerIterator.next();
			 mMap.animateCamera(CameraUpdateFactory.newLatLng(current.getPosition()), 2000, this);
			 current.showInfoWindow();
			 }
	}
	
	public GoogleMap getMap(){
		return mMap;
	}
	
	public List<Marker> getList(){
		return mMarkerList;
	}
	
	
	//reads off the file and stores in the list
	private void stringToMarker(ArrayList<String[]> list){
        if(list.size() == 0) return;
        LatLng position = null;
        String[] str = null;
        Iterator<String[]> it = list.iterator();
        while(it.hasNext()) {
            str = it.next();
            position = new LatLng(Double.parseDouble(str[0]), Double.parseDouble(str[1]));
            Marker added = mMap.addMarker(new MarkerOptions()
					.position(position)
                    .title(str[2])
					.snippet(str[3])
					//.icon(BitmapDescriptorFactory.fromPath(this.markerPng))
					);
            this.mMarkerList.add(added);

        }

    }
	
	// Function to add a proximity alert to each location
	private void addProximityAlert(double lat, double lng) {
		Intent proximityIntent = new Intent(PROXIMITY_ALERT_INTENT);
		PendingIntent proximityPendingIntent = PendingIntent.getBroadcast(
				getApplicationContext(), 0, proximityIntent, 0);
		locationManager.addProximityAlert(
				lat, lng, RADIUS, -1, proximityPendingIntent);
		IntentFilter proximityFilter = new IntentFilter(PROXIMITY_ALERT_INTENT); 
		registerReceiver(mBroadcastReceiver, proximityFilter);

	}
	// Broadcast receiver to receive the broadcast sent from LocationManager
	private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		public void onReceive(Context argContext, Intent argIntent) {
			
			// key determines whether user is entering (true) or exiting (false)
	        final String key = LocationManager.KEY_PROXIMITY_ENTERING;
	        final Boolean entering = argIntent.getBooleanExtra(key, false);

	        if (entering) {
	            Toast.makeText(argContext, "Entering", Toast.LENGTH_SHORT).show();
	        } 
	        else {
	            Toast.makeText(argContext, "Exiting", Toast.LENGTH_SHORT).show();
	        }
		}
	};

}
