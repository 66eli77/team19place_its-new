package com.example.googlemapdemo.test;

import com.example.Team19PlaceIts.MainActivity;

import android.test.ActivityInstrumentationTestCase2;

public class GoogleMapTest extends
		ActivityInstrumentationTestCase2<MainActivity> {
	
	private MainActivity mActivity;
	
	public GoogleMapTest() {
		super(MainActivity.class);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	  protected void setUp() throws Exception {
	    super.setUp();

	    setActivityInitialTouchMode(false);

	    mActivity = getActivity();
	}
	
	 public void testPreConditions() {
		    // add anything you define as preconditions for this test
		  } // end of testPreConditions() method definition
	 
	 public void testMap(){
		 // this is to check if we successfully obtain a google map. and it passed.
		 assertTrue(mActivity.getMap() != null);
	 }
	 
	 public void testMarker(){
		 // add your code for icon here.
	 }
	 
	 public void testList(){
		 // add your code for list here.
		 assertEquals(mActivity.getList().size(), 0);
	 }
	 
}
